package palindrome;

import java.util.Stack;
import java.util.Scanner;

public class palin {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
	    String string = input.nextLine();
	    Stack<Character> stack = new Stack<Character>();
	    
	    for (int i = 0; i < string.length(); i++) {
	        stack.push(string.charAt(i));
	    }

	    String reverseString = "";

	    while (!stack.isEmpty()) {
	    	reverseString += stack.pop();
	    }

	    if (string.equals(reverseString))
	        System.out.println("palindsrome");
	    else
	        System.out.println("isn't palindrome");

	}
}

