package oop;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		edge pentagon = new edge();
		pentagon.n = 31;
		pentagon.edge_length = 25;
		pentagon.angle();
		pentagon.area();
		pentagon.circumference();
		System.out.println(pentagon.circumference);
		System.out.println(pentagon.area);
		System.out.println(pentagon.angle);
		
		parallel_edge diamond = new parallel_edge();
		diamond.angle1 = 50;
		diamond.angle2 = 130;
		diamond.edge_length = 5;
		diamond.area();
		diamond.circumference();
		System.out.println(diamond.circumference);
		System.out.println(diamond.area);
		
	}

}
