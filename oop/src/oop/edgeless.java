package oop;

public class edgeless extends shape {
	double radius;
	
	void area () {
		area = Math.PI * radius;
	}
	
	void circumference() {
		circumference = 2 * Math.PI * radius;
	}

}
